def repoUrl="https://gitlab.com/Rvooradi123/rv-cicd.git"
def sonarUrl="http://172.31.44.84:9000"
def nexusRepoUrl="http://172.31.35.243:8081/repository/maven-releases"
if (!env.RELEASE_VERSION)  {
    error('Release Version Input is needed to proceed')         
}

node() {

    stage('Clone Repo') {
        git credentialsId: 'Gitlab', url: repoUrl
    }

    stage('Code Compile') {
        sh """
            mvn versions:set -DnewVersion=${env.RELEASE_VERSION}-RELEASE
            mvn clean compile
        """
    }

    stage('Code Quality Check') {
        sh """
            mvn versions:set -DnewVersion=${env.RELEASE_VERSION}-RELEASE
            mvn sonar:sonar -Dsonar.host.url=${sonarUrl} -Dsonar.login=09493943e16a3eedee24b0d07032de7cde7236ef
        """
    }

    stage('Package Jar File') {
        sh """
            mvn versions:set -DnewVersion=${env.RELEASE_VERSION}-RELEASE
            mvn package
        """
    }

    stage('Deploy War file to Nexus') {
        sh """
            GROUP_ID=`echo -e 'setns x=http://maven.apache.org/POM/4.0.0\ncat /x:project/x:groupId/text()' | xmllint --shell pom.xml | grep -v /`
            ARTIFACT_ID=`echo -e 'setns x=http://maven.apache.org/POM/4.0.0\ncat /x:project/x:artifactId/text()' | xmllint --shell pom.xml | grep -v /`
            mvn versions:set -DnewVersion=${env.RELEASE_VERSION}-RELEASE
            mvn -s .settings.xml deploy:deploy-file -DgroupId=\${GROUP_ID} -DartifactId=\${ARTIFACT_ID} -Dversion=${env.RELEASE_VERSION}-RELEASE -DgeneratePom=true -Dpackaging=jar -DrepositoryId=nexus -Durl=${nexusRepoUrl} -Dfile=target/spring-boot-mysql-springdatajpa-hibernate-${env.RELEASE_VERSION}-RELEASE.jar
        """
    }

    stage('Approval') {
        def userInput = true
        def didTimeout = false
        try {
            timeout(time: 150, unit: 'SECONDS') { 
                userInput = input(
                id: 'Proceed1', message: 'Was this successful?', parameters: [
                [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Do you want to deploy']
                ])
            }
            env.action=true
        } catch(err) { 
            def user = err.getCauses()[0].getUser()
            if('SYSTEM' == user.toString()) { 
                didTimeout = true 
            } else {
                userInput = false
                echo "Aborted by: [${user}]"
            }
            env.action=false
        }
    }

    stage('Deploy') {
        if (action) {
            def URL="http://35.203.184.85:8081/repository/maven-releases/netgloo/spring-boot-mysql-springdatajpa-hibernate/1.5-RELEASE/spring-boot-mysql-springdatajpa-hibernate-1.5-RELEASE.jar"
            sh """
                ansible-playbook -i hosts deploy.yml -u centos -e URL=${nexusRepoUrl}/netgloo/spring-boot-mysql-springdatajpa-hibernate/${env.RELEASE_VERSION}-RELEASE/spring-boot-mysql-springdatajpa-hibernate-${env.RELEASE_VERSION}-RELEASE.jar
            """
        } else {
            println 'Skipping Deployment'
        }
    }
}