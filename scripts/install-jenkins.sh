#!/bin/bash

if [ $(id -u) -ne 0 ]; then 
    echo "You should run this script as root user"
    exit 1
fi 

sed -i -e '/^SELINUX/ c SELINUX=permissive' /etc/selinux/config

type yum &>/dev/null 
if [ $? -ne 0 ]; then
    echo "Script will work only on RedHat Family OS (RHEL, CentOS, OEL, Aamazon Linux)"
    exit 1
fi

echo "Installing Java"
yum install maven wget ansible -y &>/dev/null 
if [ $? -ne 0 ]; then 
    echo "Java installation failure"
fi

echo "Installing Jenkins"
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo &>/dev/null
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key &>/dev/null
yum install jenkins -y &>/dev/null 
if [ $? -ne 0 ]; then 
    echo -e "Jenkins Installation Failure"
    exit 1
fi 

systemctl enable jenkins &>/dev/null  
systemctl start jenkins  &>/dev/null 
if [ $? -ne 0 ]; then 
    echo "Jenkins Startup failure"
    exit 1
fi 

sleep 30

echo -e "\e[33m You can Open jenkins in following URL"
echo -e "http://$(curl -s ifconfig.co):8080"

echo -e "\n\e[31m INITIAL PASSWORD = $(cat /var/lib/jenkins/secrets/initialAdminPassword)\e[0m"